from constructs import Construct
from aws_cdk import (
    Duration,
    Stack,
    aws_iam as iam,
    aws_sqs as sqs,
    aws_sns as sns,
    aws_sns_subscriptions as subs,
    aws_dynamodb as _dynamodb,
    aws_kinesis as _kinesis,
    aws_s3 as _s3,
    aws_lambda as _lambda,
    aws_apigateway as _apigateway
)


# make an S3 bucket that enables versioning and encrption



class CdkWorkshopStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # queue = sqs.Queue(
        #     self, "CdkWorkshopQueue",
        #     visibility_timeout=Duration.seconds(300),
        # )

        # topic = sns.Topic(
        #     self, "CdkWorkshopTopic"
        # )

        # topic.add_subscription(subs.SqsSubscription(queue))
        my_table = _dynamodb.Table(self,id='dynamoTable', table_name = 'testcdktable',
                                   partition_key = _dynamodb.Attribute(name='lastname', type=_dynamodb.AttributeType.STRING)
                                   )        
        my_stream = _kinesis.Stream(self, id='kinesisStream', stream_name='testcdkstream')
        my_bucket = _s3.Bucket(self, id='s3Bucket', bucket_name='xzhbucket',
                               versioned=True,
                               encryption=_s3.BucketEncryption.S3_MANAGED)
        # Add bucket properties like versioning and encryption
        # my_bucket.add_property('BucketEncryption', 'SSE-S3')
        # my_bucket.add_property('VersioningConfiguration', 'Enabled')
        
        my_lambda = _lambda.Function(self, id='lambdaFunction', runtime=_lambda.Runtime.PYTHON_3_9, 
                                     handler='hello.handler',
                                     code=_lambda.Code.from_asset('lambdacode'),
                                    )
        my_api = _apigateway.LambdaRestApi(self, id='lambdaApi', rest_api_name='cdkapi', handler=my_lambda)
        api_with_method = _apigateway.RestApi(self,id='restapi',rest_api_name='cdkrestapi_music')
        music = api_with_method.root.add_resource('music')
        music.add_method('GET') 
        music.add_method("DELETE", _apigateway.HttpIntegration("http://aws.amazon.com"))


        