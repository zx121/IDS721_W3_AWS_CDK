# IDS721_W3_AWS_CDK

## Project Description

- Create an S3 Bucket using CDK with AWS CodeWhisperer. 
- Requirements
    - Create S3 bucket using AWS CDK
    - Use CodeWhisperer to generate CDK code
    - Add bucket properties like versioning and encryption

## Steps


1. Open [AWS CodeCatalyst](https://codecatalyst.aws/) and create a new space and project
2. Select `Vscode` as the editor to open the project
3. Configure the AWS IAM role for the project
4. Add necessary permissions to the IAM role
5. Open the environment in the Vscode
6. Initialize the project using the following command:
    ```bash
    cdk init app --language python
    ```
7. Add features like S3 bucket, versioning, and encryption to the cdk stack file
You can use CodeWhisperer with comments like # use aws cdk to Add bucket properties like versioning and encryption in Python, and then the code will generate automatically.
8. To synthesize a CDK app, use the `cdk synth` command. This will output the following CloudFormation template:
```json
Resources:
s3Bucket6575FF6:
    Type: AWS::S3::Bucket
    Properties:
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
      BucketName: xzhbucket
      VersioningConfiguration:
        Status: Enabled
    UpdateReplacePolicy: Retain
    DeletionPolicy: Retain
    Metadata:
      aws:cdk:path: cdk-workshop/s3Bucket/Resource
```
9. You can use the cdk bootstrap command to install the bootstrap stack into an environment:
    ```bash
    cdk bootstrap
    ```    

    The first time you deploy an AWS CDK app into an environment (account/region), you’ll need to install a “bootstrap stack”. This stack includes resources that are needed for the toolkit’s operation. For example, the stack includes an S3 bucket that is used to store templates and assets during the deployment process.


10. To deploy the CDK app, use the `cdk deploy` command.

## Results
CDK apps are deployed through AWS CloudFormation. Each CDK stack maps 1:1 with CloudFormation stack. This means that you can use the AWS CloudFormation console in order to manage your stacks.

Let’s take a look at the AWS CloudFormation console. You will likely see something like this (if you don’t, make sure you are in the correct region):

![Alt text](image.png)

Go to S3 bucket and check the properties of the bucket. You will see that versioning and encryption are enabled.
![Alt text](image-1.png)
![Alt text](image-2.png)
![Alt text](image-3.png)

I also tried some other features like lambda function, API Gateway, and DynamoDB.
![Alt text](image-4.png)
![Alt text](image-5.png)

## References
- [AWS CDK](https://docs.aws.amazon.com/cdk/latest/guide/home.html)
- [AWS CDK Workshop](https://cdkworkshop.com/)
- [AWS CDK API Reference](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_s3/Bucket.html)


